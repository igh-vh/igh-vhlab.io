#!/bin/sh
set -e

echo "<h1>Handbücher</h1>" >> public/de/index.html
echo "<h1>Handbooks</h1>" >> public/en/index.html

function lang_string() {
  if [ "$1" == "de" ] ; then
    case $2 in
      "de") 
        echo "Deutsch"
        ;;
      "en") 
        echo "Englisch"
        ;;
      "fr")
        echo "Französisch"
        ;;
      *)
        exit -5
    esac
  elif [ "$1" == "en" ] ; then
    case $2 in
      "de") 
        echo "German"
        ;;
      "en") 
        echo "English"
        ;;
      "fr")
        echo "French"
        ;;
      *)
        exit -5
    esac
  else
    exit -5
  fi
}

grep -v -E '^(\#.*)?$' latex_repos.txt | while IFS=$'\t\n' read project version_major branch_name filename lang link_title; do
  if [ -z $project ]; then
    continue
  fi
  dir=$lang/latex/$project/$version_major
  mkdir -p public/$dir
  wget -O public/$dir/$project.pdf $CI_SERVER_URL/$CI_PROJECT_NAMESPACE/$project/-/jobs/artifacts/$branch_name/raw/latex/$filename?job=latex
  for page_lang in de en; do
    echo "  <p><a href=\"/$dir/$project.pdf\">$link_title ($(lang_string $page_lang $lang))</a></p>" >> public/$page_lang/index.html
  done
done
