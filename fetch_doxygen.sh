#!/bin/sh
set -e

echo "<h1>Doxygen Online-Dokumentation</h1>" >> public/de/index.html
echo "<h1>Doxygen Online docs</h1>" >> public/en/index.html

grep -v -E '^(\#.*)?$' doxygen_repos.txt | while IFS=$'\t\n' read project version_major branch_name link_title; do
  if [ -z $project ]; then
    continue
  fi
  mkdir tmp; cd tmp
  wget -O artifacts.zip $CI_SERVER_URL/$CI_PROJECT_NAMESPACE/$project/-/jobs/artifacts/$branch_name/download?job=doxygen
  unzip artifacts.zip
  mkdir -p ../public/en/doxygen/$project/
  mv html ../public/en/doxygen/$project/$version_major
  cd ..; rm -rf tmp
  for lang in de en; do
    echo "  <p><a href=\"/en/doxygen/$project/$version_major/index.html\">$link_title</a></p>" >> public/$lang/index.html
  done
done
